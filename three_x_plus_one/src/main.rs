use std::io;

fn main() {
	println!("Conject my Collatz ;)");

	loop {
		let mut x = String::new();
		let mut n: u128;

		println!("Input any positive integer:");

		io::stdin()
			.read_line(&mut x)
			.expect("Failed to read line :/");

		let x: u128 =
			match x.trim().parse() {
				Ok(num) => num,
				Err(_) => {
					println!("Your guess needs to be a positive integer, which, unfortunately, `{}` is not;", x.trim());
					continue;
				}
			};

		n = x;

		loop {
			if n == 1 {
				println!("We have reached the number 1, exiting...");
				break;
			}
			if n % 2 == 0 {
				n = n / 2;
			} else {
				//n = ((3 * n) + 1) / 2;
				n = (3 * n) + 1;
			}
		}
		break;
	}
}
