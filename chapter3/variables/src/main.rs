fn main() {
	// mutable variables
	let mut x = 5;
	println!("The value of x is: {x}");
	x = 6;
	println!("The value of x is: {x}");

	// constants
	const THREE_HOURS_IN_SECONDS: u32 = 60 * 60 * 3;
	println!("The value of THREE_HOURS_IN_SECONDS is: {THREE_HOURS_IN_SECONDS}");

	// shadowing
	let x = 5;

	let x = x + 1;

	{
		let x = x * 2;
		println!("The value of x in the inner scope is: {x}");
	}

	println!("The value of x is: {x}");

	// shadowing with different type
	let spaces = "   ";
	let _spaces = spaces.len();

	// numeric ops

	// addition
	let _sum = 5 + 10;

	// subtraction
	let _difference = 95.5 - 4.3;

	// multiplication
	let _product = 4 * 30;

	// division
	let _quotient = 56.7 / 32.2;
	let _truncated = -5 / 3; // Results in -1

	// remainder
	let _remainder = 43 % 5;

	// other data types

	// boolean
	let _t = true;

	let _f: bool = false; // with explicit type annotation

	// char
	let _c = 'z';
	let _z: char = 'ℤ'; // with explicit type annotation
	let _heart_eyed_cat = '😻';

	// tuple
	let tup: (i32, f64, u8) = (500, 6.4, 1); // it's fixed size
	let (_x, y, _z) = tup; // destructuring
	println!("The value of y is: {y}");
	let w = tup.0; // access using index
	println!("The value of w is: {w}");

	// array
	let a: [i32; 5] = [1, 2, 3, 4, 5]; // it's fixed length, use vector for growable lists

	let _first = a[0];
	let _second = a[1];

	let _b = [3; 5]; // short version of let b = [3, 3, 3, 3, 3];

	let _months = [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December",
	];
}
