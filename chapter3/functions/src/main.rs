fn main() {
	// Statements are instructions that perform some action and do not return a value.
	// Expressions evaluate to a resultant value. Let’s look at some examples.

	println!("Hello, world!");

	another_function(5);

	print_labeled_measurement(9, 'h');

	let y = {
		let x = 4;
		x + 1 //no semicolor so x+1 remains an expression instead of becoming a statement
	};

	println!("The value of y is: {}", y);

	let n = five();

	println!("The value of n is: {}", n);

	let p = plus_one(2);

	println!("The value of p is: {}", p);
}

fn another_function(x: i32) {
	println!("The value of x is: {}", x);
}

fn print_labeled_measurement(value: i32, unit_label: char) {
	println!("The measurement is: {}{}", value, unit_label);
}

fn five() -> i32 {
	7 //7 without semicolon is an expression so it returns its value
}

fn plus_one(x: i32) -> i32 {
	x + 1
}
